# QZQP Service Response Prueba

Servicio que prueba los diferentes casos con los que cuenta el nuevo catálogo ServiceResponse de la arquitectura a partir de la versión 5.3.X.

## Casos de prueba:

- ServiceResponse
	- Devolverá HttpStatus 200 excepto si la petición es POST, en cuyo caso devuelve 201
	- Devuelve un body

- ServiceResponseCreated
	- Devolverá siempre HttpStatus 201
	- Devuelve un body

- ServiceResponseCreatedWithNoContent
	- Devolverá siempre HttpStatus 201
	- No devuelve body

- ServiceResponseNoContent
	- Devolverá siempre HttpStatus 204
	- No devuelve body

- ServiceResponseOK
	- Devolverá siempre HttpStatus 200, independientemente del método usado
	- Será utilizado para cubrir los casos de los casos especiales, denominados controllers