package com.bbva.qzqp.serviceresponseprueba.facade.v01;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponseCreated;
import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponseCreatedWithNoContent;
import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponseNoContent;
import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponseOK;

public interface ISrvServiceResponsePruebaV01 {

	ServiceResponse getPrueba();

	ServiceResponse postPrueba();

	ServiceResponseCreated putCreatedPrueba();

	ServiceResponseCreatedWithNoContent putCreatedNoContentPrueba();

	ServiceResponseNoContent deleteNoContentPrueba();

	ServiceResponseOK patchOkPrueba();
}