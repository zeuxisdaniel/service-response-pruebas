package com.bbva.qzqp.serviceresponseprueba.facade.v01.dto;

import com.bbva.jee.arq.spring.core.servicing.annotations.IdEntity;
import org.codehaus.jackson.annotate.JsonProperty;

@SuppressWarnings("unchecked")
public class DtoPrueba {
	
	@IdEntity
	private String rfc;
	@JsonProperty("BusinessUpdateData")
	private String nombre;
	@SuppressWarnings("unused")
	private Integer edad;

	public String getNombre()
	{
		return this.nombre;
	}

	@SuppressWarnings("unused")
	public void setNombre(String nombre)
	{
		this.nombre = nombre;
	}

	public Integer getEdad()
	{
		return this.edad;
	}

	public void setEdad(Integer edad)
	{
		this.edad = edad;
	}

	public String getRfc()
	{
		return this.rfc;
	}

	public void setRfc(String rfc)
	{
		this.rfc = rfc;
	}
}