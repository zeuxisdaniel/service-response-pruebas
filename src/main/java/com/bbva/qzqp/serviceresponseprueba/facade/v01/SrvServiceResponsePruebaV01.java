package com.bbva.qzqp.serviceresponseprueba.facade.v01;


import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponseCreated;
import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponseCreatedWithNoContent;
import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponseNoContent;
import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponseOK;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.bbva.qzqp.serviceresponseprueba.facade.v01.dto.DtoPrueba;
import org.springframework.stereotype.Service;
import com.bbva.jee.arq.spring.core.servicing.annotations.SMC;
import com.bbva.jee.arq.spring.core.servicing.annotations.SN;
import com.bbva.jee.arq.spring.core.servicing.annotations.VN;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;
import com.wordnik.swagger.jaxrs.PATCH;

import java.util.List;
import java.util.ArrayList;
/*Pruebas de sonarqube*/
// Not null
import org.hibernate.validator.internal.constraintvalidators.bv.NotNullValidator;
//Proxy connector
import java.lang.reflect.Proxy;
// No oficial hazelcast
import com.hazelcast.util.CollectionUtil;
// NO java IO
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.Serializable;
import java.io.InputStream;
// No connector rest rule
import java.net.HttpURLConnection;
import org.apache.commons.httpclient.HttpClient;
// import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.IOException;

@SuppressWarnings("unchecked")
@Path("/v1")
@SN(registryID="--SN-prueba--",logicalID="serviceresponseprueba")
@VN(vnn="v1")
@Api(value="/serviceresponseprueba/v1",description="Servicio Trucha para probar el nuevo catálogo Service Response")
@Service
public class SrvServiceResponsePruebaV01 implements ISrvServiceResponsePruebaV01 {

	private static final String PATH = "/serviceresponseprueba";
	private static final String PATHCREATEDWITHNOCONTENT = "/serviceresponseprueba/createdWithNoContent";
	private static final String PATHCREATED = "/serviceresponseprueba/created";
	private static final String PATHNOCONTENT = "/serviceresponseprueba/noContent";
	private static final String PATHOK = "/serviceresponseprueba/ok";
	private static final String MENSAJE1 = "aliasGCE1";
	private static final String MENSAJE2 = "aliasGCE2";
	private static final String MENSAJE3 = "Found Sucessfully";
	@SuppressWarnings("unused")
	private static final String MENSAJE4 = "Technical Error";
	private List<DtoPrueba> dataResponse = new ArrayList<>();

	private static final Logger LOGGER = LoggerFactory.getLogger(SrvServiceResponsePruebaV01.class);
	private static final ObjectMapper MAPPER = new ObjectMapper();
	protected DtoPrueba dtoPrueba = new DtoPrueba();

	@SuppressWarnings("divzero")
	@GET
	@Path(PATH)
	@ApiOperation(value="Get Prueba", notes="Service Response normal, genera estatus 200 excepto POST (201) y devuelve Body",response=ServiceResponse.class)
	@ApiResponses(value = {
		@ApiResponse(code = -1, message = MENSAJE1),
		@ApiResponse(code = -1, message = MENSAJE2),
		@ApiResponse(code = 200, message = MENSAJE3, response=ServiceResponse.class),
		@ApiResponse(code = 500, message = MENSAJE4)})
	@SMC(registryID="--SMC-prueba--",logicalID="getPrueba")
	// Probando regla ProducesAnnotation
	// @Produces({MediaType.APPLICATION_JSON})
	@Override
	public ServiceResponse getPrueba() {
		// generarDatosPrueba();
		InputStream is = getClass().getResourceAsStream("dto/app_properties.json");
		is = this.class.getClassLoader().getResourceAsStream("dto/app_properties.json");
		pruebaBbva();
		// return ServiceResponse.data(this.dataResponse).build();
		return ServiceResponse.data(this.dtoPrueba).build();
	}

	@POST
	@Path(PATH)
	@ApiOperation(value="Post Prueba", notes="Service Response normal, genera estátus 200 excepto POST (201) y devuelve Body",response=ServiceResponse.class)
	@ApiResponses(value = {
		@ApiResponse(code = -1, message = MENSAJE1),
		@ApiResponse(code = -1, message = MENSAJE2),
		@ApiResponse(code = 200, message = MENSAJE3, response=ServiceResponse.class),
		@ApiResponse(code = 500, message = MENSAJE4)})
	@SMC(registryID="--SMC-prueba--",logicalID="postPrueba")
	@Produces({MediaType.APPLICATION_JSON})
	@Override
	public ServiceResponse postPrueba() {
		return ServiceResponse.data(this.generarDtoSalida()).build();
	}

	@PUT
	@Path(PATHCREATED)
	@ApiOperation(value="Service Response Created Prueba", notes="Utiliza clase ServiceResponseCreated, devuelve siempre estátus 201 y body",response=ServiceResponseCreated.class)
	@ApiResponses(value = {
		@ApiResponse(code = -1, message = MENSAJE1),
		@ApiResponse(code = -1, message = MENSAJE2),
		@ApiResponse(code = 200, message = MENSAJE3, response=ServiceResponseCreated.class),
		@ApiResponse(code = 500, message = MENSAJE4)})
	@SMC(registryID="--SMC-prueba--",logicalID="putCreatedPrueba")
	@Produces({MediaType.APPLICATION_JSON})
	@Override
	public ServiceResponseCreated putCreatedPrueba() {
		return ServiceResponseCreated.data(this.generarDtoSalida()).build();
	}

	@PUT
	@Path(PATHCREATEDWITHNOCONTENT)
	@ApiOperation(value="Service Response Created With No Content Prueba", notes="Utiliza clase ServiceResponseCreatedWithNoContent, devuelve siempre estátus 201 sin contenido o body",response=ServiceResponseCreatedWithNoContent.class)
	@ApiResponses(value = {
		@ApiResponse(code = -1, message = MENSAJE1),
		@ApiResponse(code = -1, message = MENSAJE2),
		@ApiResponse(code = 200, message = MENSAJE3, response=ServiceResponseCreatedWithNoContent.class),
		@ApiResponse(code = 500, message = MENSAJE4)})
	@SMC(registryID="--SMC-prueba--",logicalID="putCreatedNoContentPrueba")
	@Produces({MediaType.APPLICATION_JSON})
	@Override
	public ServiceResponseCreatedWithNoContent putCreatedNoContentPrueba() {
		return ServiceResponseCreatedWithNoContent.ServiceResponseCreatedWithNoContentBuilder.build();
	}

	@DELETE
	@Path(PATHNOCONTENT)
	@ApiOperation(value="Service Response No Content Prueba", notes="Utiliza clase ServiceResponseNoContent, devuelve siempre estátus 204 sin contenido o body",response=ServiceResponseNoContent.class)
	@ApiResponses(value = {
		@ApiResponse(code = -1, message = MENSAJE1),
		@ApiResponse(code = -1, message = MENSAJE2),
		@ApiResponse(code = 200, message = MENSAJE3, response=ServiceResponseNoContent.class),
		@ApiResponse(code = 500, message = MENSAJE4)})
	@SMC(registryID="--SMC-prueba--",logicalID="deleteNoContentPrueba")
	@Produces({MediaType.APPLICATION_JSON})
	@Override
	public ServiceResponseNoContent deleteNoContentPrueba() {
		return ServiceResponseNoContent.ServiceResponseNoContentBuilder.build();
	}

	@PATCH
	@Path(PATHOK)
	@ApiOperation(value="Service Response Ok Prueba", notes="Utiliza clase ServiceResponseOK, devuelve siempre estátus 200 y body",response=ServiceResponseOK.class)
	@ApiResponses(value = {
		@ApiResponse(code = -1, message = MENSAJE1),
		@ApiResponse(code = -1, message = MENSAJE2),
		@ApiResponse(code = 200, message = MENSAJE3, response=ServiceResponseOK.class),
		@ApiResponse(code = 500, message = MENSAJE4)})
	@SMC(registryID="--SMC-prueba--",logicalID="patchOkPrueba")
	@Produces({MediaType.APPLICATION_JSON})
	@Override
	public ServiceResponseOK patchOkPrueba() {
		return ServiceResponseOK.data(this.generarDtoSalida()).build();
	}

	// Métodos para generar datos de prueba
	public void generarDatosPrueba(){
		DtoPrueba datoPrueba1 = new DtoPrueba();
		DtoPrueba datoPrueba2 = new DtoPrueba();
		datoPrueba1.setRfc("ABC123");
		datoPrueba1.setNombre("Prueba1");
		datoPrueba1.setEdad(new Integer(10));
		datoPrueba2.setRfc("XYZ789");
		datoPrueba2.setNombre("Prueba2");
		datoPrueba2.setEdad(new Integer(3));
		this.dataResponse.add(datoPrueba1);
		this.dataResponse.add(datoPrueba2);
	}

	public DtoPrueba generarDtoSalida(){
		DtoPrueba datoPrueba = new DtoPrueba();
		datoPrueba.setRfc("XYZ789");
		datoPrueba.setNombre("PruebaDto");
		datoPrueba.setEdad(new Integer(10));
		return datoPrueba;
	}

	/*Prueba de sonar*/
	public void pruebaBbva(){
		// probando regla NoSystemPropertiesUse
		// System.getProperty("clave");
		// System.getProperties();
		// System.setProperty("clave", "valor");
		// System.setProperties(null);
		this.dtoPrueba.setRfc("RFCprueba");
		this.dtoPrueba.setNombre("nombre");
		this.dtoPrueba.setEdad(new Integer(69));
		try {
			LOGGER.info("request (JSON): {}", MAPPER.writeValueAsString(dtoPrueba));
		} catch (IOException e) {
			LOGGER.error(e.getMessage(), e);
		}
	}
}